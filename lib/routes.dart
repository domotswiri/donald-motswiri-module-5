// ignore_for_file: prefer_const_constructors
import 'package:e_commerce_ui/screens/splash/loading_screen.dart';
import 'package:e_commerce_ui/screens/todo/todo_screen.dart';
import 'package:flutter/widgets.dart';

final Map<String, WidgetBuilder> routes = {
  LoadingScreen.routeName: (context) => LoadingScreen(),
  TodoScreen.routeName: (context) => TodoScreen(),
};
