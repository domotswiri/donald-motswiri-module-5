// ignore_for_file: prefer_const_constructors

import 'package:e_commerce_ui/constants.dart';
import 'package:e_commerce_ui/routes.dart';
import 'package:e_commerce_ui/screens/splash/loading_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
    // name: 'MTN App Academy Web',
    options: const FirebaseOptions(
      apiKey: "AIzaSyBgaux5iYum9cEJ2esnnILxGQPy0ecOtKo",
      authDomain: "mtn-app-academy-2d704.firebaseapp.com",
      projectId: "mtn-app-academy-2d704",
      storageBucket: "mtn-app-academy-2d704.appspot.com",
      messagingSenderId: "391799225013",
      appId: "1:391799225013:web:68d66e6272bb94c4ee1cfc",
      measurementId: "G-0HFB051B6T",
    ),
  );

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flexi eShop',
      theme: theme(),
      home: const LoadingScreen(),
      // initialRoute: SplashScreen.routeName,
      initialRoute: LoadingScreen.routeName,
      routes: routes,
    );
  }
}

ThemeData theme() {
  return ThemeData(
    scaffoldBackgroundColor: Colors.white,
    fontFamily: 'Muli',
    primaryColor: kPrimaryColor,
    appBarTheme: appBarTheme(),
    textTheme: textTheme(),
    inputDecorationTheme: inputDecorationTheme(),
    visualDensity: VisualDensity.adaptivePlatformDensity,
  );
}

TextTheme textTheme() {
  return const TextTheme(
    bodyText1: TextStyle(color: kTextColor),
    bodyText2: TextStyle(color: kTextColor),
  );
}

AppBarTheme appBarTheme() {
  return AppBarTheme(
    color: Colors.white,
    elevation: 0,
    systemOverlayStyle: SystemUiOverlayStyle.light,
    iconTheme: IconThemeData(color: Colors.black),
    textTheme: TextTheme(
      headline6: TextStyle(color: Color(0xFF8B8B8B), fontSize: 18),
    ),
    foregroundColor: Color(0xFF8B8B8B),
  );
}

InputDecorationTheme inputDecorationTheme() {
  OutlineInputBorder outlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10),
    borderSide: BorderSide(color: kTextColor),
    gapPadding: 10,
  );

  return InputDecorationTheme(
    floatingLabelBehavior: FloatingLabelBehavior.always,
    contentPadding: EdgeInsets.symmetric(
      horizontal: 42,
      vertical: 20,
    ),
    enabledBorder: outlineInputBorder,
    focusedBorder: outlineInputBorder,
    border: outlineInputBorder,
  );
}
