# donald-motswiri-module-5

M5-Assessment 1

## Instructions:
Set up a Firebase account.
Create a Gitlab account.
Create a repository on Gitlab as name-surname-module-5

### Assessment:
All the code must be submitted through a Gitlab repository link. 

1. Create a project on Firebase
1. Connect your web app to Firebase
1. Add a form to your app with at least 3 input fields
1. Write code to connect your app to the database to create, read, update and delete